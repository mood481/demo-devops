#!/bin/bash
# First we check if there is enough space if not we'll send a message

BACKUP_DIR=/var/opt/gitlab/backups
TMP_FILE=/tmp/gitlab-$(date +"%d%m%Y").tar.gz

FREE=$(/usr/bin/gdrive about | grep Free | cut -f 2 -d ":" | cut -f 1 -d ".")
FREE=$((FREE + 0))
echo $FREE
if (( $FREE > 1 ))
then

  tar -cf $BACKUP_DIR/$(date "+etc-gitlab-%s.tar") -C / etc/gitlab && gitlab-rake gitlab:backup:create && tar -zcvf $TMP_FILE $BACKUP_DIR && gdrive upload -p 0B4Qsg3AQNBwtWlByVVFENzViSm8 --delete $TMP_FILE && rm -rf /var/opt/gitlab/backups/* && exit 0 ||  echo "Se produjo un error, quiza debamos lanzar el backup a mano" | mail -s "Gitlab Macto - Mensaje de error al realizar la copia de seguridad" mcabrerizo@artemit.com.es

else
   echo "Aviso - Queda menos de 1 GB en Google Drive para los backups del VPS GitLab." | mail -s "Gitlab Macto - Mensaje de aviso de espacio disponible" mcabrerizo@artemit.com.es
fi

